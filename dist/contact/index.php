<?php
session_start();
header("Cache-control: private"); 
header_remove("Expires"); 
header_remove("Cache-Control"); 
header_remove("Pragma"); 
header_remove("Last-Modified");
ob_start();
include_once('../app_config.php');
include(APP_PATH.'libs/head.php');
?>
<meta name = "format-detection" content = "telephone=no">

<link type="text/css" rel="stylesheet" href="<?php echo APP_ASSETS ?>css/exvalidation.css">

<!-- Anti spam part1: the contact form start -->
</head>
<body id="contact">
<!-- header Part ==================================================
================================================== -->
<?php include(APP_PATH.'libs/header.php'); ?>
<?php include(APP_PATH.'libs/gNavi.php'); ?>
<!-- header Part ==================================================
================================================== -->
<h2 class="bHead"><span><img src="<?php echo APP_ASSETS; ?>img/contact/bHead.svg" alt="お問い合わせCONTACT"></span></h2>

<?php

// 設定
require(APP_PATH."libs/form/jphpmailer.php");
$script = "";
$gtime = time();

//always keep this
$action = htmlspecialchars($_POST['action']);
$reg_url = htmlspecialchars($_POST['url']);
//end always keep this


//お問い合わせフォーム内容
$reg_name = htmlspecialchars($_POST['nameuser']);
$reg_furi = htmlspecialchars($_POST['furi']);
$reg_email = htmlspecialchars($_POST['email']);
$reg_add2 = htmlspecialchars($_POST['add2']);
$reg_zipcode1 = htmlspecialchars($_POST['zipcode1']);
$reg_zipcode2 = htmlspecialchars($_POST['zipcode2']);
$reg_add = htmlspecialchars($_POST['add']);
$reg_topic = $_POST['topic'];

$strCheckbox = "";
for($i=0; $i < count($reg_topic); $i++)
{
	$strCheckbox .= $reg_topic[$i].", ";
}
if($strCheckbox != "") $strCheckbox = substr($strCheckbox,0,strlen($string)-2);

// $reg_checkAll01 = $_POST['checkAll01'];

$reg_content = htmlspecialchars($_POST['content']);
?>
 <div id="wrap">
    <!-- Main Content
    ================================================== -->
    <main>
        	
	<?php if($action == "confirm"){
	
	require("step2.php");

	}elseif($action == "send"){
		
		require("step3.php");
		
	}else{

		require("step1.php");

	}
	?>
    </main>
</div><!-- #wrap -->

<!--Footer ==================================================
================================================== -->

<?php include(APP_PATH.'libs/footer.php'); ?>
<script type="text/javascript" src="<?php echo APP_ASSETS; ?>js/form/ajaxzip3.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo APP_ASSETS; ?>js/form/jquery.cookie.js"></script>
<script type="text/javascript" src="<?php echo APP_ASSETS; ?>js/form/exvalidation.js"></script>
<script type="text/javascript" src="<?php echo APP_ASSETS; ?>js/form/exchecker-ja.js"></script>
<script type="text/javascript">
	$(function(){
	  $("form.form-1").exValidation({
	    rules: {
			nameuser: "chkrequired",
			furi: "chkrequired chkromanji",
			email: "chkrequired chkemail",
			cemail: "chkrequired",
			zipcode1: "chkrequired",
			zipcode2: "chkrequired",
			add2: "chkrequired",
			add: "chkrequired"
	    },
	    stepValidation: true,
	    scrollToErr: true,
		errHoverHide: true,
		firstValidate: true,
		customListener     : "blur"
	  });
	});
</script>
<script>
	jQuery(document).ready(function($) {
		$(".radioStyle label").click(function(){
			var id = $(this).attr('for');
			if($("#" + id).is(":checked")) $("#" + id).prop('checked', false);
			else $("#" + id).prop('checked', true);
			return false;
		});
	    $('.clearform').click(function(){
	        $('.form-1')[0].reset();
	    })
	});
</script>
</body>
</html>