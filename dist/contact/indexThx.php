<?php
session_start();
include_once('../app_config.php');
if(!empty($_SESSION['fromContact'])) unset($_SESSION['fromContact']);
else header('location: '.APP_URL);
include(APP_PATH."libs/head.php");
?>
<meta http-equiv="refresh" content="15; url=<?php echo APP_URL ?>">
<script type="text/javascript">
history.pushState({ page: 1 }, "title 1", "#noback");
window.onhashchange = function (event) {
	window.location.hash = "#noback";
};
</script>
</head>
<body id="contact">
<!--Header-->
<?php include(APP_PATH."libs/header.php") ?>
<?php include(APP_PATH.'libs/gNavi.php'); ?>
<!-- header Part ==================================================
================================================== -->
<h2 class="bHead"><span><img src="<?php echo APP_ASSETS; ?>img/contact/bHead.svg" alt="お問い合わせCONTACT"></span></h2>
<div id="wrap">
<!-- Main Content
================================================== -->
	<main>
		<div class="indexThx">
			<h3>送信が完了いたしました。</h3>
			<p>お問い合わせありがとうございます。<br>後日、担当よりご連絡させていただきます。<br />3日以内に弊社より連絡がない場合はお手数ですが、再送信もしくは直接ご連絡くださいますようお願い致します。</p>
			<p><a href="<?php echo APP_URL;?>">←TOPへ戻る</a></p>
		</div>
	</main>
</div>
<!--Footer ==================================================
================================================== -->
<?php include(APP_PATH.'libs/footer.php') ?>
<!-- End Document
================================================== -->
	</body>
</html>