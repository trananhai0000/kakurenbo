<?php
if(!defined("APP_URL")){
	include('../wp/wp-load.php');
	get_template_part( 404 );
	exit();
}
?>
<div class="contactIntro">
	<h3><img src="<?php echo APP_ASSETS; ?>img/contact/ttl_contact.svg" alt="スポパルへのお問い合わせ"></h3>
	<p><small>お電話でのお問い合わせはこちら</small><a href="tel:084-959-3388">TEL.084-959-3388</a>営業時間 月〜土 9：00〜24：00／日・祝 9：00〜21：00 定休日 木曜日</p>
	<p><em>WEBメールフォームでのお問い合わせは<br class="sp">以下よりご入力ください。</em>下記送信フォームに必要事項をご記入の上、お問い合わせください。<br>半角カナ、絵文字、機種依存文字は文字化けする可能性がありますので<br class="sp">ご入力をお控えください。</p>
</div>
<form method="post" class="form-1" action="<?php echo $script ?>?<?php echo $gtime ?>" name="form1" onSubmit="return check()">
<p class="txtTable">※は必須入力項目となっております。</p>

	<p class="hid_url">Leave this empty: <input type="text" name="url"></p><!-- Anti spam part1: the contact form -->
	<table class="tableContact" cellspacing="0">
			<tr>
				<th>お名前<em>※</em></th>
				<td><em class="sp">※</em><input type="text" name="nameuser" id="nameuser" class="size1"><small>様</small></td>
			</tr>
			<tr>
				<th>フリガナ<em>※</em></th>
				<td><em class="sp">※</em><input type="text" name="furi" id="furi" class="size1"><small>サマ</small></td>
			</tr>
			<tr>
				<th>メールアドレス<em>※</em></th>
				<td><em class="sp">※</em><input type="email" name="email" id="email"></td>
			</tr>
			<tr>
				<th>確認のためもう一度<em>※</em></th>
				<td><em class="sp">※</em><input type="email"  name="cemail" id="cemail" value=""></td>
			</tr>
			<tr>
				<th>ご連絡先<em>※</em></th>
				<td><em class="sp">※</em><input type="text"  name="add2" id="add2" value=""></td>
			</tr>
			<tr>
				<th>郵便番号<em>※</em></th>
				<td><em class="sp">※</em>〒 <input type="text" name="zipcode1" id="zipcode1" class="size2" size="4" maxlength="3"> ー <input type="text" name="zipcode2" class="size3" id="zipcode2" onKeyUp="AjaxZip3.zip2addr('zipcode1','zipcode2','add','add')" ><i>↓自動入力されます。</i></td>
			</tr>
			<tr>
				<th>ご住所<em>※</em></th>
				<td><em class="sp">※</em><input type="text"  name="add" id="add" value=""></td>
			</tr>
			<tr>
				<th>トピックをお選びください<em>&nbsp;</em><span class="sp">※複数選択可</span></th>
				<td>
				<span class="radioStyle">
					<input type="checkbox" id="topic1" name="topic[]" value="ご入会"><label for="topic1">ご入会</label><input type="checkbox" id="topic2" name="topic[]" value="見学・体験"><label for="topic2">見学・体験</label><input type="checkbox" id="topic3" name="topic[]" value="料金"><label for="topic3">料金</label><input type="checkbox" id="topic4" name="topic[]" value="マシン"><label for="topic4">マシン</label><input type="checkbox" id="topic5" name="topic[]" value="スタジオプログラム"><label for="topic5">スタジオプログラム</label><br><input type="checkbox" id="topic6" name="topic[]" value="パーソナルトレーニング"><label for="topic6">パーソナルトレーニング</label><input type="checkbox" id="topic7" name="topic[]" value="求人"><label for="topic7">求人</label><input type="checkbox" id="topic8" name="topic[]" value="その他"><label for="topic8">その他</label><i>※複数選択可</i>
				</span>
				</td>
			</tr>
			<tr>
				<th>お問い合わせ内容<em>&nbsp;</em></th>
				<td><textarea name="content" id="content"></textarea></td>
			</tr>
			<tr>
				<th>送信確認<em>※</em></th>
				<td><em class="sp">※</em>
				<span class="chkcheckbox radioStyle radioStyle2" id="radioarray02">
					<input type="checkbox" id="rdSend" name="rdSend" value="上記内容をご確認後、お間違えなければチェックを入れてください。">
					<label for="rdSend">上記内容をご確認後、お間違えなければチェックを入れてください。</label>
				</span>
				</td>
			</tr>
		</table>
		<div class="txtContact">
			<p>※お問い合わせの際に必要な個人情報または、お問い合わせの内容等につきましては、お問い合わせの回答の目的のみに利用し、　当社プライバシーポリシーに則って、適正かつ厳重に管理いたします。</p>
		</div>
        <p class="btn_verify"><button name="action" value="send" class="confirm"><span>送信する</span></button><button class="clearform" name="clearform" value="cancel"><span>キャンセル</span></button></p>
        <!-- <p class="btnBack"><a href="<?php echo APP_URL; ?>"><img src="<?php echo APP_ASSETS; ?>img/contact/btn_back.svg" alt="back"></a></p> -->
</div>
</form>