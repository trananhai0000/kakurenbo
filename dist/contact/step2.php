<?php
if(!defined("APP_URL")){
	include('../wp/wp-load.php');
	get_template_part( 404 );
	exit();
}
//======================================================================================== お問い合わせ確認画面

	$br_reg_content = nl2br($reg_content);
?>
<form method="post" class="form-1" action="<?php echo $script ?>?<?php echo $gtime ?>" >
<div class="formBlock container">
	<p class="hid_url">Leave this empty: <input type="text" name="url" value="<?php echo $reg_url ?>"></p><!-- Anti spam part1: the contact form -->
	<table class="tableContact" cellspacing="0">
		<tr>
			<th>お名前</th>
			<td><?php echo $reg_name ?>様</td>
		</tr>
		<tr>
			<th>フリガナ</th>
			<td><?php echo $reg_furi ?>サマ</td>
		</tr>
		<tr>
			<th>メールアドレス</th>
			<td><?php echo $reg_email ?></td>
		</tr>
		<tr>
			<th>ご連絡先</th>
			<td><?php echo $reg_add2 ?></td>
		</tr>
		<tr>
			<th>郵便番号</th>
			<td>〒 <?php echo $reg_zipcode1 ?> ー <?php echo $reg_zipcode2 ?></td>
		</tr>
		<tr>
			<th>ご住所</th>
			<td><?php echo $reg_add ?></td>
		</tr>
		<?php if(!empty($reg_topic)){ ?>
		<tr>
			<th>トピックをお選びください</th>
			<td><?php
				$strCheckbox = "";
				for($i=0; $i < count($reg_topic); $i++)
				{
					$strCheckbox .= $reg_topic[$i].", ";
				}
				if($strCheckbox != "") $strCheckbox = substr($strCheckbox,0,strlen($string)-2);
				echo $strCheckbox;
				?></td>
		</tr>
		<?php } ?>
		<?php if(!empty($reg_content)){ ?>
		<tr>
			<th>お問い合わせ内容</th>
			<td><?php echo $br_reg_content ?></td>
		</tr>
		<?php } ?>
	</table>
<input type="hidden" name="nameuser" value="<?php echo $reg_name ?>">
<input type="hidden" name="furi" value="<?php echo $reg_furi ?>">
<input type="hidden" name="email" value="<?php echo $reg_email ?>">
<input type="hidden" name="add2" value="<?php echo $reg_add2 ?>">
<input type="hidden" name="zipcode1" value="<?php echo $reg_zipcode1 ?>">
<input type="hidden" name="zipcode2" value="<?php echo $reg_zipcode2 ?>">
<input type="hidden" name="add" value="<?php echo $reg_add ?>">
<input type="hidden" name="topic" value="<?php echo $reg_topic ?>">
<input type="hidden" name="checkAll01" value="<?php echo $strCheckbox ?>">
<input type="hidden" name="content" value="<?php echo $reg_content ?>">
<!-- always keep this -->
<input type="hidden" name="url" value="<?php echo $reg_url ?>">
<!-- end always keep this -->

<p class="formBack"><a href="javascript:history.back()">入力内容を修正する</a></p>
<p class="btn_verify"><button name="action" value="send" class="confirm"><span>この内容で送信する</span></button></p>
</div>
</form> 