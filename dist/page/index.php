<?php
// Author: A+LIVE
include_once('../app_config.php');
include(APP_PATH.'libs/head.php');
?>
</head>

<body id="page" class="page">
    <!-- Header
    ================================================== -->
    <?php include(APP_PATH.'libs/header.php'); ?>
	<?php include(APP_PATH.'libs/gNavi.php'); ?>

    <div id="wrap">
        <!-- Main Content
        ================================================== -->
        <main>
            
        </main>

    </div><!-- #wrap -->

    <!-- Footer
    ================================================== -->
    <?php include(APP_PATH.'libs/footer.php'); ?>
<!-- End Document
================================================== -->
</body>
</html>
