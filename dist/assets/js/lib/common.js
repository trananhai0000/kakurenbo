/*-----------------------------------------------------------
jquery-rollover.js
jquery-opacity-rollover.js
-------------------------------------------------------------*/

/*-----------------------------------------------------------
jquery-rollover.js　※「_on」画像を作成し、class="over"を付ければOK
-------------------------------------------------------------*/

function initRollOverImages() {
  var image_cache = new Object();
  $("img.over").each(function(i) {
    var imgsrc = this.src;
    var dot = this.src.lastIndexOf('.');
    var imgsrc_on = this.src.substr(0, dot) + '_on' + this.src.substr(dot, 4);
    image_cache[this.src] = new Image();
    image_cache[this.src].src = imgsrc_on;
    $(this).hover(
      function() { this.src = imgsrc_on; },
      function() { this.src = imgsrc; });
  });
}

$(document).ready(initRollOverImages);

/*-----------------------------------------------------------
jquery-opacity-rollover.js　※class="opa"を付ければOK
-------------------------------------------------------------*/

$(document).ready(function(){
	$("img.opa").fadeTo(0,1.0);
	$("img.opa").hover(function(){
	$(this).fadeTo(200,0.5);
	},
	function(){
	$(this).fadeTo(200,1.0);
	});
}); 


/* common */
$(function(){
	$('.hamburger').click(function(){
		$(this).toggleClass('active');
		$('body').toggleClass('layerOn');
	});
});


$('#pagetop a').click(function () {
	$('body,html').animate({
		scrollTop: 0
	}, 500);
	return false;
});


