<div class="imgFooter"><div class="container"><img src="<?php echo APP_ASSETS; ?>img/common/bg_footer.svg" alt=""></div></div>
<footer class="footer">
    <div class="container">
    	<p id="pagetop"><a href="javascript:;"><img src="<?php echo APP_ASSETS; ?>img/common/ico_scroll_top.svg" alt=""></a></p>
    	<div class="footerTop clearfix">
            <div class="left">
                <h4>かくれんぼ なごや福祉ネット SEOテキスト(仮)</h4>
                <p>(仮文章です)介護福祉士（かいごふくしし、英: Certified Care Worker）は、社会福祉士及び介護福祉士法を根拠とする国家資格。福祉系では、社会福祉士・精神保健福祉士・保育士と並ぶ、名称独占資格の国家資格である（場合によっては、名称に福祉士が入る三資格を、福祉系三大国家資格（通称：三福祉士）と呼ぶこともある）。社会福祉士及び介護福祉士法で位置づけられた、社会福祉業務（身体介護・生活援助など）に携わる人の国家資格である。介護福祉士は、病院・介護老人保健施設・特別養護老人ホーム・デイケアセンターや障害福祉サービス事業所等の社会福祉施設が活動場所となることが多い。社会福祉士及び介護福祉士法で位置づけられた。</p>
            </div>
            <div class="right">
                <h4>なごや福祉ネット</h4>
                <p class="address">〒462-0047<br>愛知県名古屋市北区金城町四丁目47番地</p>
                <div class="contactFooter">
                    <p class="phoneBox"><strong class="ffO">052-918-7410</strong><span class="textTime">電話受付 月曜〜金曜</span><span class="time ffO">09:00〜17:00</span></p>
                    <p class="mailBox"><a href="<?php echo APP_URL; ?>contact/"><span>お問い合わせ</span></a></p>
                </div>
            </div>  
        </div>
        <ul class="fNav clearfix">
            <li><a href="<?php echo APP_URL; ?>">TOP</a></li>
            <li><a href="<?php echo APP_URL; ?>about/">なごや福祉ネットとは</a></li>
            <li><a href="<?php echo APP_URL; ?>find/">お仕事情報</a></li>
            <li><a href="<?php echo APP_URL; ?>membership/">入会について</a></li>
            <li><a href="<?php echo APP_URL; ?>member/">会員一覧</a></li>
            <li><a href="<?php echo APP_URL; ?>blog/">ブログ</a></li>
            <li><a href="<?php echo APP_URL; ?>blog/">お問い合わせ</a></li>
        </ul>
    </div>
</footer>
<p class="copyright ffO">&copy; 2018 特定非営利活動法人かくれんぼ</p>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo APP_ASSETS; ?>js/lib/jquery1-12-4.min.js"><\/script>')</script>
<script type="text/javascript" src="<?php echo APP_ASSETS; ?>js/lib/common.js"></script>
<script type="text/javascript" src="<?php echo APP_ASSETS; ?>js/lib/smoothscroll.js"></script>
<script type="text/javascript" src="<?php echo APP_ASSETS; ?>js/lib/jquery.matchHeight.min.js"></script>

