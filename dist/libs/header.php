<!-- Google Tag Manager -->
<!-- End Google Tag Manager -->
<header class="header">
    <div class="container clearfix">
    	<h1 id="logo"><a href="<?php echo APP_URL; ?>"><img src="<?php echo APP_ASSETS; ?>img/common/main_logo.svg" alt="なごや福祉ネット"></a></h1>
    	<div class="rightHeader">
    		<div class="contactHeader">
    			<p class="phoneBox"><strong class="ffO">052-918-7410</strong><span class="textTime">電話受付 月曜〜金曜</span><span class="time ffO">09:00〜17:00</span></p><p class="mailBox"><a href="<?php echo APP_URL; ?>contact/"><span>お問い合わせ</span></a></p>
    		</div>
    		<div class="mainMenu">
	    		<ul class="itemNav clearfix">
		            <li><a href="<?php echo APP_URL; ?>">TOP</a></li>
		            <li><a href="<?php echo APP_URL; ?>about/">なごや福祉ネットとは</a></li>
		            <li><a href="<?php echo APP_URL; ?>find/">お仕事情報</a></li>
		            <li><a href="<?php echo APP_URL; ?>membership/">入会について</a></li>
		            <li><a href="<?php echo APP_URL; ?>member/">会員一覧</a></li>
		            <li><a href="<?php echo APP_URL; ?>blog/">ブログ</a></li>
		        </ul>
	        </div>
    	</div>
        
    </div>
</header>
