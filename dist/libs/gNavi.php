<div class="gNaviWrap layerMenu">
    <div class="inLayer">
        <ul class="gNavi clearfix">
            <li class="gHome"><a href="<?php echo APP_URL; ?>"><span>ホーム</span></a></li>
            <li class="gAbout"><a href="<?php echo APP_URL; ?>about/"><img src="<?php echo APP_ASSETS; ?>img/common/gnavi_about.png" alt="ABOUT SPOPAL スポパルとは" class="over"><span>スポパルとは</span></a></li>
            <li class="gStudio"><a href="<?php echo APP_URL; ?>studio/"><img src="<?php echo APP_ASSETS; ?>img/common/gnavi_studio.png" alt="STUDIO PROGRAM スタジオプログラム" class="over"><span>スタジオプログラム</span></a></li>
            <li class="gKid"><a href="<?php echo APP_URL; ?>kids/"><img src="<?php echo APP_ASSETS; ?>img/common/gnavi_kid.png" alt="KIDS PROGRAM キッズプログラム" class="over"><span>キッズプログラム</span></a></li>
            <li class="gMachine"><a href="<?php echo APP_URL; ?>machine/"><img src="<?php echo APP_ASSETS; ?>img/common/gnavi_machine.png" alt="MACHINE EXERCISE マシーンエクササイズ" class="over"><span>マシーンエクササイズ</span></a></li>
            <li class="gTrial"><a href="<?php echo APP_URL; ?>freetrial/"><img src="<?php echo APP_ASSETS; ?>img/common/gnavi_trial.png" alt="FREE TRIAL 無料体験・ご見学" class="over"><span>無料体験・ご見学</span></a></li>
            <li class="gJoin"><a href="<?php echo APP_URL; ?>joinus/"><img src="<?php echo APP_ASSETS; ?>img/common/gnavi_join.png" alt="JOIN US! 今すぐ入会！" class="over"><span>今すぐ入会！</span></a></li>
        </ul>
    </div>
</div>