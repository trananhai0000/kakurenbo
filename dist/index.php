<?php
// Author: A+LIVE
include_once('app_config.php');
include(APP_PATH.'libs/head.php');
?>
</head>

<body id="top" class="top">
    <!-- Header
    ================================================== -->
    <?php include(APP_PATH.'libs/header.php'); ?>
    
    <div id="wrap">
        <!-- Main Content
        ================================================== -->
        <main>
            <div class="mainBanner">
                <img src="<?php echo APP_ASSETS; ?>img/top/main_banner.jpg" alt="みんなが安心して暮らせる町づくり">
                <p class="waveBg"><img src="<?php echo APP_ASSETS; ?>img/top/bg_wave.png" alt=""></p>
                <div class="contentBanner">
                    <p>名古屋市高齢者日常生活支援研修修了者の方へ</p>
                    <h2 class="">みんなが安心して<br>暮らせる町づくり</h2>
                    <a href="<?php echo APP_URL; ?>about/">なごや福祉ネットとは</a>
                </div>
            </div>
            <div class="topics container">
                <div class="content">
                    <h2 class="ffO">topics</h2>
                    <ul>
                        <li>
                            <p><a href=""><span class="time">2018.03.19</span><span class="cat">お知らせ</span><strong class="title">上飯田福祉会館　平成30年度 講座受講生募集のお知らせ</strong></a></p>
                        </li>
                        <li>
                            <p><a href=""><span class="time">2017.12.01</span><span class="cat catOrange">カテゴリー</span><strong class="title">「高齢者日常生活支援研修」の受講生を募集しております。</strong></a></p>
                        </li>
                        <li>
                            <p><a href=""><span class="time">2017.11.23</span><span class="cat catOrange">カテゴリー</span><strong class="title">【通信課程】介護福祉士実務者研修H30.4月生募集しております</strong></a></p>
                        </li>
                        <li>
                            <p><a href=""><span class="time">2017.11.10</span><span class="cat">お知らせ</span><strong class="title">介護サービスのブログ更新しました！</strong></a></p>
                        </li>
                        <li>
                            <p><a href=""><span class="time">2017.11.05</span><span class="cat catOrange">カテゴリー</span><strong class="title">ハローステーションからのお知らせを更新しました！</strong></a></p>
                        </li>
                    </ul>
                    <p class="loadMoreBtn"><a href="<?php echo APP_URL; ?>blog/">TOPIC一覧を見る</a></p>
                </div>
            </div>
            <div class="aboutBox">
                <h2 class="titleBox"><img src="<?php echo APP_ASSETS; ?>img/top/img_title01.png" alt="なごや福祉ネットのできること"></h2>
                <div class="container">
                    <ul class="clearfix">
                        <li>
                            <p class="imgFeatured"><img src="<?php echo APP_ASSETS; ?>img/top/img_about01.jpg" alt=""></p>
                            <div class="content">
                                <p class="title"><span>名古屋市高齢者<br>日常生活支援研修</span></p>
                                <p class="desc">(仮文章)今、ケアを必要とする高齢者を地域が一体となって支える福祉が求められています。ぜひ、この研修で介護の基礎知識を身につけませんか？</p>
                                <p class="loadMoreBtn"><a href="<?php echo APP_URL; ?>blog/">詳細を見る</a></p>
                            </div>
                        </li>
                        <li>
                            <p class="imgFeatured"><img src="<?php echo APP_ASSETS; ?>img/top/img_about02.png" alt=""></p>
                            <div class="content">
                                <p class="title"><span>会員サポート（勉強会）</span></p>
                                <p class="desc">(仮文章)今、ケアを必要とする高齢者を地域が一体となって支える福祉が求められています。ぜひ、この研修で介護の基礎知識を身につけませんか？</p>
                                <p class="loadMoreBtn"><a href="<?php echo APP_URL; ?>blog/">詳細を見る</a></p>
                            </div>
                        </li>
                        <li>
                            <p class="imgFeatured"><img src="<?php echo APP_ASSETS; ?>img/top/img_about03.jpg" alt=""></p>
                            <div class="content">
                                <p class="title"><span>行政との意見交換</span></p>
                                <p class="desc">(仮文章)今、ケアを必要とする高齢者を地域が一体となって支える福祉が求められています。ぜひ、この研修で介護の基礎知識を身につけませんか？</p>
                                <p class="loadMoreBtn"><a href="<?php echo APP_URL; ?>blog/">詳細を見る</a></p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="relatedSite">
                <h2 class="titleBox"><img src="<?php echo APP_ASSETS; ?>img/top/img_title02.png" alt="なごや福祉ネットのできること"></h2>
                <div class="container">
                    <ul>
                        <li>
                            <a href=""><img src="<?php echo APP_ASSETS; ?>img/top/img_related01.png" alt=""></a>
                        </li>
                        <li>
                            <a href=""><img src="<?php echo APP_ASSETS; ?>img/top/img_related02.png" alt=""></a>
                        </li>
                        <li>
                            <a href=""><img src="<?php echo APP_ASSETS; ?>img/top/img_related03.png" alt=""></a>
                        </li>
                        <li>
                            <a href=""><img src="<?php echo APP_ASSETS; ?>img/top/img_related01.png" alt=""></a>
                        </li>
                        <li>
                            <a href=""><img src="<?php echo APP_ASSETS; ?>img/top/img_related02.png" alt=""></a>
                        </li>
                        <li>
                            <a href=""><img src="<?php echo APP_ASSETS; ?>img/top/img_related03.png" alt=""></a>
                        </li>
                        <li>
                            <a href=""><img src="<?php echo APP_ASSETS; ?>img/top/img_related01.png" alt=""></a>
                        </li>
                        <li>
                            <a href=""><img src="<?php echo APP_ASSETS; ?>img/top/img_related02.png" alt=""></a>
                        </li>
                        <li>
                            <a href=""><img src="<?php echo APP_ASSETS; ?>img/top/img_related03.png" alt=""></a>
                        </li>
                    </ul>
                </div>
            </div>
        </main>

    </div><!-- #wrap -->

    <!-- Footer
    ================================================== -->
    <?php include(APP_PATH.'libs/footer.php'); ?>
    <script type="text/javascript" src="<?php echo APP_ASSETS; ?>js/lib/slick.min.js"></script>

    <script>
        $('.relatedSite ul').on("init", function(e, slick) {
            var countSlideShow = slick.options.slidesToShow;
            if(countSlideShow <= 3) {
                $('.relatedSite .container').css('max-width', '720px');
            }
        })
        .slick({
            dots: false,
            slidesToShow: 5,
            autoplay: true,
            prevArrow: '<img class="prevArrow" src="<?php echo APP_ASSETS; ?>img/top/ico_prev_orange.svg">',
            nextArrow: '<img class="nextArrow" src="<?php echo APP_ASSETS; ?>img/top/ico_next_orange.svg">',
        });


        $(window).on("load resize", function(slick) {
            $('.aboutBox ul li .content .title').matchHeight();
        })


    </script>
<!-- End Document
================================================== -->
</body>
</html>
